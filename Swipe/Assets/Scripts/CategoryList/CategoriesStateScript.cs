﻿using Assets.Scripts.src;
using Assets.Scripts.src.Models;
using Assets.Scripts.src.Repository;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CategoriesStateScript : MonoBehaviour {

    private List<Category> categories;

	// Use this for initialization
	void Start () {
        categories = Repository.GetCategories();
        int passed = categories.Where(category => category.State == State.PASSED).Count();
        GetComponent<Text>().text = passed + "/" + categories.Count;
	}
}
