﻿using Assets.Scripts.src;
using Assets.Scripts.src.Models;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using Assets.Scripts.src.Repository;

public class CategoryItemScript : MonoBehaviour {

    public Category category;
    
    public void SetCategory(Category category)
    {
        this.category = category;
        GetComponentsInChildren<Image>()[2].sprite = category.Sprite;
        foreach (TextMeshProUGUI text in GetComponentsInChildren<TextMeshProUGUI>())
        {
            if (text.name.Equals("TextPassed")) text.gameObject.SetActive(category.State.Equals(State.PASSED));
            if (text.name.Equals("TextAvaliable")) text.gameObject.SetActive(category.State.Equals(State.UNLOCKED));
            if (text.name.Equals("TextDisable")) text.gameObject.SetActive(category.State.Equals(State.LOCKED) || !category.IsLoaded);
            text.text = category.Title;
        }
        if (!category.State.Equals(State.LOCKED))
        {
            GetComponentsInChildren<Image>()[3].gameObject.SetActive(false);
            if (category.IsLoaded)
                GetComponentsInChildren<Image>()[3].gameObject.SetActive(false);
        }
        else if (category.IsLoaded)
            GetComponentsInChildren<Image>()[4].gameObject.SetActive(false);

        if (category.State.Equals(State.LOCKED) || !category.IsLoaded)
        {
            GetComponentsInChildren<Image>()[2].gameObject.SetActive(false);
        }
        
        GetComponentInChildren<Button>().onClick.AddListener(delegate
        {
            Selected.category = category;
            if (Repository.GotSubcategories(category))
            {
                SceneManager.LoadScene("SubcategoryListScene", LoadSceneMode.Single);
            }
            else
            {
                Selected.subcategory = null;
                SceneManager.LoadScene("LevelListScene", LoadSceneMode.Single);
            }
        });
    }
}
