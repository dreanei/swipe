﻿using Assets.Scripts.src.Repository;
using UnityEngine;


public class CategoryListScript : MonoBehaviour
{
    public CategoryItemScript ItemPrefab;

    private void Start()
    {
        var itemHeight = Screen.width / 4f;
        var categories = Repository.GetCategories();

        var rect = GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, itemHeight  * categories.Count);
        transform.position = Vector2.zero;

        for (int i = 0; i < categories.Count; i++)
        {
            var category = categories[i];
            var item = Instantiate(ItemPrefab.gameObject, transform, true);
            var offset = -itemHeight * i - itemHeight/2f;
            item.transform.position = new Vector3(
                    item.transform.position.x,
                    offset,
                    item.transform.position.z
                );
            item.GetComponent<CategoryItemScript>().SetCategory(category);
        }
        ItemPrefab.gameObject.SetActive(false);
    }
}
