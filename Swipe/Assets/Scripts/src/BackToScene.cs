﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToScene : MonoBehaviour {

    public string scene;
	
	void Update () {
        if (Input.GetKey(KeyCode.Escape))
            SceneManager.LoadScene(scene, LoadSceneMode.Single);
	}
}
