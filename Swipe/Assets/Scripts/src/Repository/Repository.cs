﻿using Assets.Scripts.src.Models;
using Assets.Scripts.src.MyHtmlParser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.src.Repository
{
    public class Repository
    {
        private const string CATEGORY_FILE_NAME = "category.json";

        public static List<Category> GetCategories()
        {
            List<Category> categories = new List<Category>();
            categories.AddRange(GetLocalCategories());
            categories.AddRange(GetRemoteCategories(categories));
            return categories;
        }

        public static Category GetCategory(string categoryFolderName)
        {
            string dir = Application.streamingAssetsPath + "\\preset\\" + categoryFolderName;
            var logoFile = new WWW(dir + "\\logo.jpg");
            while (!logoFile.isDone) { }
            var texture = logoFile.texture;
            var sprite = Sprite.Create(texture as Texture2D, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
            var res = new Category(new DirectoryInfo(dir).Name, GetStringFromJson(dir + "\\" + CATEGORY_FILE_NAME), sprite);
            res.SetState(GetState(res));
            if (Directory.GetDirectories(dir).Length == 0)
            {
                res.IsLoaded = false;
            }
            return res;
        }

        private static List<Category> GetLocalCategories()
        {
            List<Category> categories = new List<Category>();
            string path = Application.streamingAssetsPath + "\\preset";
            foreach (string dir in Directory.GetDirectories(path))
            {
                categories.Add(GetCategory(new DirectoryInfo(dir).Name));
            }
            return categories;
        }

        internal static bool GotSubcategories(Category category)
        {
            if (Directory.GetDirectories(Application.streamingAssetsPath + "\\preset\\" + category.FolderName).Count() == 0)
            {
                HtmlParser htmlParser = new HtmlParser();
                var ServerElements = htmlParser.Parse("/swipe/" + category.FolderName);
                foreach (Element element in ServerElements.Where(el => el.IsDirectory))
                {
                    if (element.Name.Split('_').Length > 1) if (element.Name.Split('_')[1].Equals("level")) return false;
                }
                return true;
            }
            return Directory.GetFiles(Directory.GetDirectories(Application.streamingAssetsPath + "\\preset\\" + category.FolderName)[0])
                .Where(file => new FileInfo(file).Name.Equals("subcategory.json"))
                .Count() > 0;
        }

        private static List<Category> GetRemoteCategories(List<Category> existingCategories)
        {
            HtmlParser htmlParser = new HtmlParser();
            var ServerCategories = htmlParser.Parse("/swipe/");
            foreach (Element element in ServerCategories.Where(el => el.IsDirectory))
            {
                if (existingCategories.Where(category => category.FolderName.Equals(element.Name)).Count() == 0)
                {
                    var jsonUrl = HtmlParser.URL + element.Href + "category.json";
                    var logoUrl = HtmlParser.URL + element.Href + "logo.jpg";
                    var folder = Application.streamingAssetsPath + "\\preset\\" + element.Name;
                    var json = Application.streamingAssetsPath + "\\preset\\" + element.Name + "\\category.json";
                    var logo = Application.streamingAssetsPath + "\\preset\\" + element.Name + "\\logo.jpg";
                    Directory.CreateDirectory(folder);
                    HtmlParser.LoadFile(jsonUrl, json);
                    HtmlParser.LoadFile(logoUrl, logo);
                }
            }
            return new List<Category>();
        }

        internal static List<Subcategory> GetSubcategories(Category category)
        {
            List<Subcategory> subcategories = new List<Subcategory>();
            if (Directory.GetDirectories(category.Path).Length == 0)
                LoadSubcategories(category);
            foreach (string dir in Directory.GetDirectories(category.Path))
            {
                var localFile = new WWW(dir + "\\logo.jpg");
                while (!localFile.isDone) { }
                var texture = localFile.texture;
                var sprite = Sprite.Create(texture as Texture2D, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
                var subc = new Subcategory(GetStringFromJson(dir + "\\subcategory.json"), sprite, new DirectoryInfo(dir).Name, category.FolderName);
                subc.State = GetState(subc);
                subc.IsLoaded = Directory.GetDirectories(dir).Length > 0;
                subcategories.Add(subc);
            }
            return subcategories;
        }

        private static void LoadSubcategories(Category category)
        {
            HtmlParser htmlParser = new HtmlParser();
            var ServerElements = htmlParser.Parse("/swipe/" + category.FolderName);
            foreach (Element element in ServerElements.Where(el => el.IsDirectory))
            {
                Directory.CreateDirectory(category.Path + "\\" + element.Name);
                var jsonUrl = HtmlParser.URL + element.Href + "/subcategory.json";
                var jsonFile = category.Path + "\\" + element.Name + "\\subcategory.json";
                var logoUrl = HtmlParser.URL + element.Href + "/logo.jpg";
                var logoFile = category.Path + "\\" + element.Name + "\\logo.jpg";
                HtmlParser.LoadFile(jsonUrl, jsonFile);
                HtmlParser.LoadFile(logoUrl, logoFile);
            }
        }

        internal static List<Level> GetLevels(Category category)
        {
            List<Level> levels = new List<Level>();
            if (Directory.GetDirectories(category.Path).Count() == 0)
                LoadLevels(category);
            foreach (string dir in Directory.GetDirectories(category.Path))
            {
                var level = new LevelBuilder()
                    .SetTitle(new DirectoryInfo(dir).Name.Split('_')[0])
                    .SetCategoryFolderName(category.FolderName)
                    .SetLevelFolderName(new DirectoryInfo(dir).Name)
                    .SetState(GetState(dir))
                    .Create();
                level.IsLoaded = Directory.GetDirectories(dir).Length > 0;
                levels.Add(level);
            }
            SortLevels(levels);
            return levels;
        }

        private static void SortLevels(List<Level> levels)
        {
            levels.Sort((l1, l2) =>
            {
                try
                {
                    if (Convert.ToInt32(l1.Title) > Convert.ToInt32(l2.Title))
                        return 1;
                    else if (Convert.ToInt32(l1.Title) == Convert.ToInt32(l2.Title))
                        return 0;
                    else
                        return -1;
                }
                catch (Exception)
                {
                    return -1;
                }
            });
        }

        internal static List<Level> GetLevels(Subcategory subcategory)
        {
            List<Level> levels = new List<Level>();
            if (Directory.GetDirectories(subcategory.Path).Count() == 0)
                LoadLevels(subcategory);
            foreach (string dir in Directory.GetDirectories(subcategory.Path))
            {
                var level = new LevelBuilder()
                    .SetTitle(new DirectoryInfo(dir).Name.Split('_')[0])
                    .SetCategoryFolderName(subcategory.CategoryFolderName)
                    .SetSubcategoryFolderName(subcategory.FolderName)
                    .SetLevelFolderName(new DirectoryInfo(dir).Name)
                    .SetState(GetState(dir))
                    .Create();
                level.IsLoaded = Directory.GetDirectories(dir).Length > 0;
                levels.Add(level);
            }
            SortLevels(levels);
            return levels;
        }

        private static void LoadLevels(Category category)
        {
            HtmlParser htmlParser = new HtmlParser();
            var ServerElements = htmlParser.Parse("/swipe/" + category.FolderName);
            foreach (Element element in ServerElements.Where(el => el.IsDirectory))
            {
                Directory.CreateDirectory(category.Path + "\\" + element.Name);
            }
        }

        private static void LoadLevels(Subcategory subcategory)
        {
            HtmlParser htmlParser = new HtmlParser();
            var url = "/swipe/" + subcategory.CategoryFolderName + "/" + subcategory.FolderName;
            var ServerElements = htmlParser.Parse(url);
            foreach (Element element in ServerElements.Where(el => el.IsDirectory))
            {
                Directory.CreateDirectory(subcategory.Path + "\\" + element.Name);
            }
        }

        internal static void LoadLevel(Level level)
        {
            HtmlParser htmlParser = new HtmlParser();
            var subc = (level.SubcategoryFolderName == "" ? "" : "/" + level.SubcategoryFolderName);
            var url = "/swipe/" + level.CategoryFolderName + subc + "/" + level.LevelFolderName;
            Debug.Log(url);
            var ServerElements = htmlParser.Parse(url);
            foreach (Element element in ServerElements.Where(el => el.IsDirectory))
            {
                Directory.CreateDirectory(level.Path + "\\" + element.Name);
                var subelements = htmlParser.Parse(element.Href);
                foreach(Element elem in subelements)
                {
                    var fileUrl = HtmlParser.URL + elem.Href;
                    var filePath = level.Path + "\\" + element.Name + "\\" + elem.Name;
                    HtmlParser.LoadFile(fileUrl, filePath);
                }
            }
            foreach (Element element in ServerElements.Where(el => !el.IsDirectory))
            {
                var fileUrl = HtmlParser.URL + element.Href;
                var filePath = level.Path + "\\" + element.Name;
                HtmlParser.LoadFile(fileUrl, filePath);
            }
        }

        internal static Level GetLevel(Level level)
        {
            List<Picture> Pictures = new List<Picture>();
            foreach (string file in Directory.GetFiles(level.Path).Where(file => new FileInfo(file).Extension.Equals(".jpg") || new FileInfo(file).Extension.Equals(".jpeg")))
            {
                var localFile = new WWW(file);
                while (!localFile.isDone) { }
                var texture = localFile.texture as Texture2D;
                TextureScale.Bilinear(texture, 512, 512);
                texture.Apply();
                var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), -(Vector2.left + Vector2.down).normalized * 0.705f);
                Pictures.Add(new Picture(new FileInfo(file).Name, sprite, file));
            }
            return new LevelBuilder()
                .SetTitle(level.LevelFolderName.Split('_')[0])
                .SetQuestion(GetStringFromJson(level.Path + "//level.json"))
                .SetCategoryFolderName(level.CategoryFolderName)
                .SetSubcategoryFolderName(level.SubcategoryFolderName)
                .SetLevelFolderName(level.LevelFolderName)
                .SetPictures(Pictures)
                .Create();

        }

        private static string GetStringFromJson(string filePath)
        {
            return GetStringFromJson(filePath, Application.systemLanguage);
        }

        private static string GetStringFromJson(string filePath, SystemLanguage language)
        {
            string data;
#if UNITY_EDITOR || UNITY_IOS
            data = File.ReadAllText(filePath);
#elif UNITY_ANDROID
                WWW dataReader = new WWW(filePath);
                while (!dataReader.isDone){}
                data = dataReader.text;
#endif
            return SText.FromJson(data).GetString(language);
        }

        internal static State GetState(Category category)
        {
            return GetState(category.Path);
        }

        internal static State GetState(Subcategory subcategory)
        {
            return GetState(subcategory.Path);
        }

        internal static State GetState(Level level)
        {
            return GetState(level.Path);
        }

        internal static State GetState(string path)
        {
            var filePath = path + "\\state";
            if (File.Exists(filePath))
            {
                var content = File.ReadAllText(filePath);
                switch (content)
                {
                    case "LOCKED":
                        return State.LOCKED;
                    case "DEFAULT":
                    case "UNLOCKED":
                        return State.UNLOCKED;
                    case "PASSED":
                        return State.PASSED;
                }
                return State.DEFAULT;
            }
            else
            {
                SetState(path, State.DEFAULT);
                return State.DEFAULT;
            }
        }

        internal static void SetState(string path, State state)
        {
            var filePath = path + "\\state";
            if (File.Exists(filePath))
            {
                File.Create(filePath).Dispose();
            }
            File.WriteAllText(filePath, state.ToString());
        }

        public static LevelState GetLevelState(Level level)
        {
            var file = level.Path + "\\levelState";
            if (!File.Exists(file))
            {
                NewLevelState(level);
                return GetLevelState(level);
            }
            string data;
#if UNITY_EDITOR || UNITY_IOS
            data = File.ReadAllText(file);
#elif UNITY_ANDROID
                WWW dataReader = new WWW(file);
                while (!dataReader.isDone){}
                data = dataReader.text;
#endif
            return JsonUtility.FromJson<LevelState>(data);
        }

        public static void SetLevelState(Level level, LevelState state)
        {

        }

        public static void NewLevelState(Level level)
        {
            var file = level.Path + "\\levelState";
            LevelState ls = new LevelState();
            foreach (string f in
                Directory.GetFiles(level.Path)
                .Where(fl => new FileInfo(fl).Extension.Equals(".jpg")))
            {
                var name = new FileInfo(f).Name.Split('.')[0];
                var title = name.Split('_')[0];
                var rightAnswerString = name.Split('_')[1];
                var rightAnswer = rightAnswerString.Equals("yes", StringComparison.InvariantCultureIgnoreCase);
                ls.AddRightAnswer(title, rightAnswer);
            }
            File.Create(file).Dispose();
            var lss = JsonUtility.ToJson(ls);
            Debug.Log(lss);
            File.WriteAllText(file, lss);
        }
    }
}
