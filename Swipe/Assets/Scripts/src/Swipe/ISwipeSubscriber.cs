﻿using UnityEngine;

namespace Assets.Scripts.src.Swipe
{
    public interface ISwipeSubscriber
    {
        void Drag(float deltaX);
        void Reset();
        void Left();
        void Right();
    }
}
