﻿using Assets.Scripts.src.Swipe;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwipeSceneSubscriber : MonoBehaviour, ISwipeSubscriber
{
    public GameObject LoadingGameObject;
    public string NextScene, PrevScene;
    public bool useNextAsPrev = false;

    private float startTime;
    public float delay;
    private string scene;
    private bool isActive = false;

    public void Update()
    {
        if (isActive && startTime + delay <= Time.time)
        {
            SceneManager.LoadScene(scene, LoadSceneMode.Single);
            isActive = false;
        }
    }

    public void Left()
    {
        if (useNextAsPrev)
        {
            if (!NextScene.Equals(string.Empty)) ToSceneWithDelay(NextScene);
        }
        else if (!PrevScene.Equals(string.Empty)) ToSceneWithDelay(PrevScene);
    }

    private void ToSceneWithDelay(string scene)
    {
        this.scene = scene;
        startTime = Time.time;
        isActive = true;
    }

    public void Right()
    {
        if (LoadingGameObject != null) LoadingGameObject.SetActive(true);
        if (!NextScene.Equals(string.Empty)) ToSceneWithDelay(NextScene);
    }

    public void Drag(float deltaX){}

    public void Reset(){}
}
