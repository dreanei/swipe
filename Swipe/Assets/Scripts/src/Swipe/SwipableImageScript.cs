﻿using Assets.Scripts.src.Swipe;
using UnityEngine;

public class SwipableImageScript : MonoBehaviour, ISwipeSubscriber
{
    public float gravity = 3f;
    public float force = 2f;
    public float delay = 1f;

    private bool IsOccupied = false;
    private float OccupyStart = 0f;
    private Vector3 startPosition;
    private Sprite nextSprite;

    private const int FORCE_MULTIPLIER = 200;
    private SpriteRenderer wrong;
    private SpriteRenderer right;

    private void Start()
    {
        nextSprite = GetComponent<SpriteRenderer>().sprite;
        force *= FORCE_MULTIPLIER;
        GetComponent<Rigidbody2D>().gravityScale = 0;
        startPosition = transform.position;
        if (transform.childCount >= 2)
        {
            if (transform.GetChild(0).GetComponent<SpriteRenderer>() != null)
                right = transform.GetChild(0).GetComponent<SpriteRenderer>();
            if (transform.GetChild(1).GetComponent<SpriteRenderer>() != null)
                wrong = transform.GetChild(1).GetComponent<SpriteRenderer>();
        }
        UpdateFilterColors();
    }

    private void Update()
    {
        if (IsOccupied && OccupyStart + delay <= Time.time)
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            GetComponent<Rigidbody2D>().angularVelocity = 0f;
            GetComponent<Rigidbody2D>().gravityScale = 0;
            transform.position = startPosition;
            transform.eulerAngles = Vector3.zero;
            GetComponent<SpriteRenderer>().sprite = nextSprite;
            IsOccupied = false;
            UpdateFilterColors();
        }
        UpdateFilters();
    }

    private void UpdateFilters()
    {
        UpdateFilter(right);
        UpdateFilter(wrong);
    }

    private void UpdateFilter(SpriteRenderer renderer)
    {
        if (renderer != null)
        {
            renderer.GetComponent<Transform>().position = new Vector3(transform.position.x, transform.position.y, -1f);
            renderer.GetComponent<Transform>().rotation = transform.rotation;
        }
    }

    private void UpdateFilterColors()
    {
        UpdateFiltersColor(wrong);
        UpdateFiltersColor(right);
    }

    private void UpdateFiltersColor(SpriteRenderer rend)
    {
        if (rend != null)
        {
            var newRColor = rend.color;
            newRColor.a = 0f;
            rend.color = newRColor;
        }
    }

    public void SetNextSprite(Sprite next) { nextSprite = next; }

    public void Right() { SideFall(Vector2.right); }

    public void Left() { SideFall(Vector2.left); }

    private void SideFall(Vector2 direction)
    {
        GetComponent<Rigidbody2D>().gravityScale = gravity;
        GetComponent<Rigidbody2D>().AddForce(direction * force);
        IsOccupied = true;
        OccupyStart = Time.time;
    }

    public void Down()
    {
        GetComponent<Rigidbody2D>().gravityScale = gravity;
        IsOccupied = true;
        OccupyStart = Time.time + 0.6f;
    }

    public void Drag(float deltaX)
    {
        if (!IsOccupied)
        {
            gameObject.transform.position = new Vector3(
                startPosition.x + deltaX * 0.8f,
                startPosition.y - Mathf.Abs(Mathf.Pow(deltaX, 2)) / Mathf.Pow(6f, 2f),
                startPosition.z
                );
            gameObject.transform.eulerAngles = new Vector3(0f, 0f, deltaX * -5f);
            if (wrong != null && right != null)
            {
                float alpha = Mathf.Min(Mathf.Abs(deltaX * 0.17f), 0.8f);
                //alpha *= 255;
                bool isRight = deltaX > 0;
                var filter = isRight ? right : wrong;
                var unusedFilter = !isRight ? right : wrong;
                var uColor = unusedFilter.color;
                uColor.a = 0f;
                unusedFilter.color = uColor;
                var newColor = filter.color;
                newColor.a = alpha;
                filter.color = newColor;
            }
        }
    }

    public void Reset()
    {
        gameObject.transform.position = startPosition;
        gameObject.transform.eulerAngles = new Vector3(0f, 0f, 0f);
        UpdateFilterColors();
    }
}
