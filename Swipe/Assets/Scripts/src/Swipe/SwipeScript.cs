﻿using UnityEngine;

namespace Assets.Scripts.src.Swipe
{
    public class SwipeScript : MonoBehaviour
    {
        public float sensitivity = 0.5f;
        public Transform[] subscribers;

        public void Update()
        {
            DetectDesctopSwipe();
            DetectSensorSwipe();
        }

        //inside class
        private Vector2 firstPressPos;
        private Vector2 secondPressPos;
        private Vector2 currentSwipe;

        private void DetectSensorSwipe()
        {
            if (Input.touches.Length > 0)
            {
                Touch t = Input.GetTouch(0);
                switch (t.phase)
                {
                    case TouchPhase.Began: firstPressPos = new Vector2(t.position.x, t.position.y); break;
                    case TouchPhase.Moved: SwipeMoved(); break;
                    case TouchPhase.Ended: SwipeEnded(); break;
                }
            }
        }

        private void DetectDesctopSwipe()
        {
            if (Input.GetMouseButtonDown(0)) firstPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            if (Input.GetMouseButton(0)) SwipeMoved();
            if (Input.GetMouseButtonUp(0)) SwipeEnded();
        }

        private void SwipeMoved()
        {
            secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
            var multiplier = 0.01f;
            currentSwipe.Scale(new Vector2(multiplier, multiplier));
            Drag();
        }

        private void SwipeEnded()
        {
            secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
            currentSwipe.Normalize();
            if (currentSwipe.x < 0 && 
                currentSwipe.y > -sensitivity && 
                currentSwipe.y < sensitivity) Left();
            else if (currentSwipe.x > 0 && 
                currentSwipe.y > -sensitivity &&
                currentSwipe.y < sensitivity) Right();
            else Reset();
        }

        public void Left()
        {
            foreach (Transform subscriber in subscribers)
            {
                subscriber.GetComponent<ISwipeSubscriber>().Left();
            }
        }

        public void Right()
        {
            foreach (Transform subscriber in subscribers)
            {
                subscriber.GetComponent<ISwipeSubscriber>().Right();
            }
        }

        private void Drag()
        {
            foreach (Transform subscriber in subscribers)
            {
                subscriber.GetComponent<ISwipeSubscriber>().Drag(currentSwipe.x);
            }
        }

        private void Reset()
        {
            foreach (Transform subscriber in subscribers)
            {
                subscriber.GetComponent<ISwipeSubscriber>().Reset();
            }
        }
    }
}
