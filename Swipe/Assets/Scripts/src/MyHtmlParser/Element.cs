﻿namespace Assets.Scripts.src.MyHtmlParser
{
    public class Element
    {
        public bool IsDirectory { get; set; }
        public string Name { get; set; }
        public string Href { get; set; }

        public Element(bool isDirectory, string name, string href = "")
        {
            IsDirectory = isDirectory;
            Name = name;
            Href = href;
        }
    }
}
