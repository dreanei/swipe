﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Assets.Scripts.src.MyHtmlParser
{
    public class HtmlParser
    {
        public static readonly string ParentDir = "[ToParentDirectory]";

        public static readonly string URL = "http://185.224.132.16";

        public List<Element> Parse(string url)
        {
            var list = new List<Element>();
            var response = Get(URL + url);
            response = response.Replace(" ", "");
            response = response.Replace("><", "> <");
            foreach (string m in response.Split(' ').Where(str => str.StartsWith("<A")))
            {
                var href = m.Split('"')[1];
                var value = m.Split('>')[1].Split('<')[0];
                if (value.Equals(ParentDir)) continue;
                Element element = new Element(value.Split('.').Length < 2, value, href);
                list.Add(element);
            }
            return list;
        }

        public static void LoadFile(string url, string file)
        {
            File.Create(file).Dispose();
            using (WebClient client = new WebClient())
            {
                client.DownloadFile(new Uri(url), file);
            }
        }

        public static string Get(string url)
        {
            string html;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                html = reader.ReadToEnd();
            }
            return html;
        }
    }
}
