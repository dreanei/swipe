﻿namespace Assets.Scripts.src.Models
{
    public enum State { PASSED, UNLOCKED, LOCKED, DEFAULT = UNLOCKED};
}
