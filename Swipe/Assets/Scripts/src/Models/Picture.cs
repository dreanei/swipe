﻿using UnityEngine;

namespace Assets.Scripts.src.Models
{
    public class Picture
    {
        public string Path { get; set; }
        public Sprite Sprite { get; set; }
        public string Title { get; set; }
        public bool Answer { get; set; }

        public Picture(string title, Sprite sprite, string path)
        {
            Title = title;
            Sprite = sprite;
            Path = path;
            Answer = title.Split('_')[1].Split('.')[0].Equals("yes");
        }
    }
}
