﻿using UnityEngine;

namespace Assets.Scripts.src.Models
{
    public class Category
    {
        public string FolderName { get; set; }
        public string Title { get; set; }
        public Sprite Sprite { get; set; }
        public State State { get; set; }
        public bool IsLoaded { get; set; }

        public string Path
        {
            get
            {
                return Application.streamingAssetsPath + "\\preset\\" + FolderName;
            }
            private set { }
        }

        public Category() {
            State = State.DEFAULT;
            IsLoaded = true;
        }

        public Category(string title) : this()
        {
            Title = title;
        }

        public Category(string title, Sprite sprite) : this(title)
        {
            Title = title;
            Sprite = sprite;
        }

        public Category(string foldername, string title, Sprite sprite) : this(title, sprite)
        {
            FolderName = foldername;
        }

        public Category SetState(State state)
        {
            State = state;
            return this;
        }

        public Category SetSprite(Sprite sprite)
        {
            Sprite = sprite;
            return this;
        }

        public Category SetTitle(string title)
        {
            Title = title;
            return this;
        }

        public Category SetFolder(string folder)
        {
            FolderName = folder;
            return this;
        }
    }
}
