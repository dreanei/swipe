﻿using UnityEngine;

namespace Assets.Scripts.src.Models
{
    public class Subcategory
    {
        public string Title { get; set; }
        public Sprite Sprite { get; set; }
        public string FolderName { get; set; }
        public string CategoryFolderName { get; set; }
        public State State { get; set; }
        public bool IsLoaded { get; set; }

        public string Path
        {
            get
            {
                return Application.streamingAssetsPath + "\\preset\\" + CategoryFolderName + "\\" + FolderName;
            }
            private set { }
        }

        public Subcategory(){
            State = State.DEFAULT;
            IsLoaded = true;
        }

        public Subcategory(string title) : this()
        {
            Title = title;
        }

        public Subcategory(string title, Sprite sprite, string folder, string categoryFolder) : this(title)
        {
            Sprite = sprite;
            FolderName = folder;
            CategoryFolderName = categoryFolder;
        }
    }
}
