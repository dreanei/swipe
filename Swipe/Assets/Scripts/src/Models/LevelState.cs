﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.src.Models
{
    [Serializable]
    public class LevelState
    {
        [SerializeField]
        Dictionary<string, bool> answers;
        [SerializeField]
        Dictionary<string, bool> rightAnswers;

        public LevelState()
        {
            answers = new Dictionary<string, bool>();
            rightAnswers = new Dictionary<string, bool>();
        }

        public LevelState AddRightAnswer(string pic, bool answer)
        {
            rightAnswers[pic] = answer;
            return this;
        }

        public LevelState AddAnswer(string pic, bool answer)
        {
            answers[pic] = answer;
            return this;
        }

        public override string ToString()
        {
            return answers.Count + "/" + rightAnswers.Count;
        }


    }
}
