﻿using System.Linq;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Assets.Scripts.src.Models
{
    class MainData
    {
        public enum StringKey{ TITLE, EXPLAIN_RIGHT_TEXT, EXPLAIN_LEFT_TEXT }

        public static readonly Dictionary<StringKey, string> FilePathes;

        static MainData()
        {
            FilePathes = new Dictionary<StringKey, string>
            {
                { StringKey.TITLE, Application.streamingAssetsPath + "\\main\\title.json" },
                { StringKey.EXPLAIN_RIGHT_TEXT, Application.streamingAssetsPath + "\\main\\explain_right.json" },
                { StringKey.EXPLAIN_LEFT_TEXT, Application.streamingAssetsPath + "\\main\\explain_left.json" }
            };
        }

        public static string Get(StringKey key)
        {
            return Get(key, Application.systemLanguage.ToString());
        }

        public static string Get(StringKey key, string lang)
        {
            string path;
            if (FilePathes.TryGetValue(key, out path))
            {
                string data;
#if UNITY_EDITOR || UNITY_IOS
                data = File.ReadAllText(path);
#elif UNITY_ANDROID
                WWW dataReader = new WWW(path);
                while (!dataReader.isDone)
                {
                }
                data = dataReader.text;
#endif
                return SText.FromJson(data).getString(lang);
            }
            else
            {
                throw new FileNotFoundException();
            }

        }
    }


}
