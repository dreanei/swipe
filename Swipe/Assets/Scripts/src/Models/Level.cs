﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.src.Models
{
    public class Level
    {
        public string Title { get; set; }
        public string Question { get; set; }
        public string CategoryFolderName { get; set; }
        public string SubcategoryFolderName { get; set; }
        public string LevelFolderName { get; set; }
        public List<Picture> Pictures { get; set; }
        public State State { get; set; }
        public bool IsLoaded { get; set; }

        public bool IsLocked
        {
            get
            {
                return State == State.LOCKED;
            }
            private set { }
        }

        public string Path
        {
            get
            {
                var subc = SubcategoryFolderName.Equals("") ? "" : "\\" + SubcategoryFolderName;
                return Application.streamingAssetsPath + "\\preset\\" + CategoryFolderName + subc + "\\" + LevelFolderName;
            }
            private set { }
        }

        public Level() {
            Pictures = new List<Picture>();
            SubcategoryFolderName = "";
            Question = "";
        }

        public Level(string title) : this()
        {
            Title = title;
        }

        public Level(string title, string categoryFolder, string SubcategoryFolder, string levelFolder): this(title)
        {
            CategoryFolderName = categoryFolder;
            SubcategoryFolderName = SubcategoryFolder;
            LevelFolderName = levelFolder;
        }

        public Level(Level level) : this(level.Title, level.CategoryFolderName, level.SubcategoryFolderName, level.LevelFolderName)
        {
            Pictures = level.Pictures;
            IsLocked = level.IsLocked;
        }
    }

    public class LevelBuilder
    {
        public string Title { get; set; }

        public string CategoryFolderName { get; set; }
        public string SubcategoryFolderName { get; set; }
        public string LevelFolderName { get; set; }
        public List<Picture> Pictures { get; set; }
        public State State { get; set; }
        public string Question { get; set; }

        public LevelBuilder()
        {
            Title = "";
            CategoryFolderName = "";
            SubcategoryFolderName = "";
            LevelFolderName = "";
            State = State.DEFAULT;
            Pictures = new List<Picture>();
        }

        public LevelBuilder SetTitle(string title)
        {
            Title = title;
            return this;
        }

        public LevelBuilder SetQuestion(string question)
        {
            Question = question;
            return this;
        }

        public LevelBuilder SetCategoryFolderName(string categoryFolder)
        {
            CategoryFolderName = categoryFolder;
            return this;
        }

        public LevelBuilder SetSubcategoryFolderName(string subcategoryFolder)
        {
            SubcategoryFolderName = subcategoryFolder;
            return this;
        }

        public LevelBuilder SetLevelFolderName(string levelFolder)
        {
            LevelFolderName = levelFolder;
            return this;
        }

        public LevelBuilder SetPictures(List<Picture> pictures)
        {
            Pictures = pictures;
            return this;
        }

        public LevelBuilder AddPicture(Picture pic)
        {
            Pictures.Add(pic);
            return this;
        }

        public LevelBuilder SetState(State state)
        {
            State = state;
            return this;
        }

        public Level Create()
        {
            var level = new Level(Title, CategoryFolderName, SubcategoryFolderName, LevelFolderName);
            level.Pictures.AddRange(Pictures);
            level.State = State;
            level.Question = Question;
            return level;
        }
    }
}
