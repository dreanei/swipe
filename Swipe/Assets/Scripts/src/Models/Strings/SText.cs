﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.src.Models
{
    [Serializable]
    class SText
    {
        [SerializeField]
        public List<SString> sStrings;

        public SText()
        {
            sStrings = new List<SString>();
        }

        public static SText FromJson(string json)
        {
            var arg = "{ \"sStrings\":" + json + "}";
            return JsonUtility.FromJson<SText>(arg);
        }

        public string GetString(SystemLanguage lang)
        {
            return sStrings.Where(str => str.country.Equals(lang.ToString())).ToArray()[0].@string.Replace("&#39;", "'"); //This is '
        }

        public string getString(string lang)
        {
            return sStrings.Where(str => str.country.Equals(lang)).ToArray()[0].@string.Replace("&#39;", "'"); //This is '
        }
    }
}
