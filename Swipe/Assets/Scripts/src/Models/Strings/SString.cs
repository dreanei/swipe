﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.src.Models
{
    [Serializable]
    class SString //Swipe STRING ;D
    {
        [SerializeField]
        public string locale;
        [SerializeField]
        public string country;
        [SerializeField]
        public string @string;
    }
}
