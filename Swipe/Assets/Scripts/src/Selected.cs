﻿using Assets.Scripts.src.Models;
using UnityEngine;

namespace Assets.Scripts.src
{
    public class Selected
    {
        public static Category category { get; set; }
        public static Subcategory subcategory { get; set; }
        public static Level level { get; set; }
        public static LevelState levelState { get
            {
                return Repository.Repository.GetLevelState(level);
            }
            set
            {
                Repository.Repository.SetLevelState(level, value);
            }
        }

        static Selected()
        {
            category = null;
            subcategory = null;
            level = null;
        }

        public static void Fix()
        {
            if (category == null) category = Repository.Repository.GetCategories()[0];
            if (subcategory == null && Repository.Repository.GotSubcategories(category))
                    subcategory = Repository.Repository.GetSubcategories(category)[0];
            if (level == null) {
                if (subcategory == null) level = Repository.Repository.GetLevel(Repository.Repository.GetLevels(category)[0]);
                else level = Repository.Repository.GetLevel(Repository.Repository.GetLevels(subcategory)[0]);
            }
        }
    }
}
