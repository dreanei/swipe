﻿using Assets.Scripts.src;
using Assets.Scripts.src.Models;
using Assets.Scripts.src.Repository;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LevelsStateScript : MonoBehaviour {

    private List<Level> levels;

    // Use this for initialization
    void Start()
    {
        if (Selected.category == null) Selected.category = Repository.GetCategories()[0];
        if (Repository.GotSubcategories(Selected.category) && Selected.subcategory == null)
            Selected.subcategory = Repository.GetSubcategories(Selected.category)[0];
        if (Selected.subcategory != null)
            levels = Repository.GetLevels(Selected.subcategory);
        else levels = Repository.GetLevels(Selected.category);
        int passed = levels.Where(level => level.State == State.PASSED).Count();
        GetComponent<Text>().text = passed + "/" + levels.Count;
    }
}
