﻿using Assets.Scripts.src;
using Assets.Scripts.src.Models;
using Assets.Scripts.src.Repository;
using System;
using System.Collections.Generic;
using UnityEngine;

public class LevelListScript : MonoBehaviour {

    public LevelItemScript ItemPrefab;

    private float offset;
    private float width;

    // Use this for initialization
    void Start()
    {
        offset = Screen.width * 0.05f;
        width = Screen.width * 0.25f;
        List<Level> levels;
        if (Selected.category == null && Selected.subcategory == null) Selected.Fix();
        if (Selected.subcategory != null)
            levels = Repository.GetLevels(Selected.subcategory);
        else levels = Repository.GetLevels(Selected.category);
        GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x, (offset + width) * (levels.Count+2) / 3);
        transform.position = Vector2.zero;

        for (int i = 0; i < levels.Count; i++)
        {
            var level = levels[i];
            var item = Instantiate(ItemPrefab.gameObject, transform, true);
            var rect = item.GetComponent<RectTransform>();
            rect.anchorMin = new Vector2(0.1f + i%3 * 0.30f, 1);
            rect.anchorMax = new Vector2(0.9f - (2 - i%3)*0.30f, 1);
            item.transform.position = new Vector3(0f, (-width - offset) * (i / 3) + (-width - offset)/2f, 0f);
            item.GetComponent<LevelItemScript>().SetLevel(level);
        }
        ItemPrefab.gameObject.SetActive(false);
    }

}
