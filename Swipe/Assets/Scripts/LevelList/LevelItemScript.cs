﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.Scripts.src;
using Assets.Scripts.src.Models;
using TMPro;
using Assets.Scripts.src.Repository;

public class LevelItemScript : MonoBehaviour {
    
    public Level level;
    public Sprite lockSprite;

    public void SetLevel(Level level)
    {
        this.level = level;
        foreach (TextMeshProUGUI text in GetComponentsInChildren<TextMeshProUGUI>())
        {
            if (text.name.Equals("TextPassed")) text.gameObject.SetActive(level.State.Equals(State.PASSED));
            else if (text.name.Equals("TextUnlocked")) text.gameObject.SetActive(level.State.Equals(State.UNLOCKED));
            else text.gameObject.SetActive(!level.IsLocked);
            text.text = level.Title;
        }

        if (!level.IsLoaded) GetComponentsInChildren<Image>()[1].gameObject.SetActive(false);

        if (!level.IsLocked)
        {
            GetComponentInChildren<Button>().onClick.AddListener(delegate
            {
                Selected.level = level;
                Repository.LoadLevel(Selected.level);
                SceneManager.LoadScene("LevelScene", LoadSceneMode.Single);
            });
        }
    }

    private void Update()
    {
        
    }
}
