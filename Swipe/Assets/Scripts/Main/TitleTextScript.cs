﻿using Assets.Scripts.src.Models;
using UnityEngine;
using UnityEngine.UI;

public class TitleTextScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Text>().text = MainData.Get(MainData.StringKey.TITLE);
	}
}
