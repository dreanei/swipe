﻿using Assets.Scripts.src.Models;
using UnityEngine;
using UnityEngine.UI;

public class ExplainTextScript : MonoBehaviour {
	
	void Start () {
        GetComponent<Text>().text = MainData.Get(MainData.StringKey.EXPLAIN_RIGHT_TEXT);
	}
}
