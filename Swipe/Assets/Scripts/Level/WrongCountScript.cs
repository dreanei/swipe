﻿using Assets.Scripts.src;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class WrongCountScript : MonoBehaviour {

    public LevelScript levelScript;

	void Update () {
        GetComponent<Text>().text = levelScript.answers.Where(answer => !answer).Count().ToString();
    }
}
