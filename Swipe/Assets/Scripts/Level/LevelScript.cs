﻿using Assets.Scripts.src;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.src.Swipe;
using Assets.Scripts.src.Models;
using Assets.Scripts.src.Repository;
using System.IO;
using System;

public class LevelScript : MonoBehaviour,  ISwipeSubscriber {

    public Level level;
    public Timer timer;

    public GameObject PictureContent;
    public GameObject QuestionPanel;
    public GameObject HelpButtons;
    public List<bool> answers;

    private bool isReady;

    public bool IsReady {
        get {
            return isReady;
        }
        set {
            PictureContent.SetActive(value);
            HelpButtons.SetActive(value);
            QuestionPanel.SetActive(!value);
            isReady = value;
        }
    }
    public SwipableImageScript image;
    public int currentPicture = 0;
    public Text questionText;

    void Start () {
        if (Selected.level == null) Selected.Fix();
        level = Selected.level.Question == null ? Repository.GetLevel(Selected.level) : Selected.level;
        level.Pictures = level.Pictures.OrderBy(pic => Convert.ToInt32(pic.Title.Split('_')[0])).ToList();
        timer.WholeTime = level.Pictures.Count * 3f;
        var ls = Repository.GetLevelState(level);
        image.GetComponent<SpriteRenderer>().sprite = level.Pictures[0].Sprite;
        QuestionPanel.GetComponentInChildren<Button>().onClick.AddListener(delegate
        {
            IsReady = true;
        });
        QuestionPanel.GetComponentInChildren<Text>().text = level.Question;
        IsReady = false;
	}
	
	void Update () {
        if (level.Pictures.Count >= currentPicture)
            image.SetNextSprite(level.Pictures[currentPicture].Sprite);
    }

    public void TimeIsOver()
    {
        image.Down();
        NextPicture();
    }

    public void MakeAnswer(bool answer = true) {
        Debug.Log("answer: " + level.Pictures[currentPicture].Answer + "(" + level.Pictures[currentPicture].Title);
        if (answer == level.Pictures[currentPicture].Answer)
        {
            answers.Add(true);
        }
        else
        {
            answers.Add(false);
        }
        var title = new FileInfo(level.Pictures[currentPicture].Path).Name.Split('_')[0];
        Selected.levelState = Selected.levelState.AddAnswer(title, answer);
    }

    public void NextPicture()
    {
        if (level.Pictures.Count - 1 > currentPicture)
            currentPicture++;
        else Finish();
        IsReady = true;
    }

    public void Finish() {

    }

    public void Left()
    {
        if (isReady)
        {
            MakeAnswer(false);
            NextPicture();
        }
    }

    public void Right()
    {
        if (isReady)
        {
            MakeAnswer();
            NextPicture();
        }
    }

    public void Drag(float deltaX){}

    public void Reset(){}
}
