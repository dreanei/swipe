﻿using UnityEngine;

public class Timer : MonoBehaviour {

    public bool IsFinished { get { return LeftTime == 0; } private set { } }

    public float LeftTime { get; private set; }

    private float wholeTime = 0f;

    public float WholeTime
    {
        get
        {
            return wholeTime;
        }

        set
        {
            wholeTime = value;
            LeftTime = value;
        }
    }

    void Update()
    {
        LeftTime = Mathf.Max(0f, LeftTime - Time.deltaTime);
    }

    public float Persent()
    {
        return LeftTime / wholeTime;
    }

    public float PersetGone()
    {
        return (wholeTime - LeftTime) / wholeTime;
    }
}
