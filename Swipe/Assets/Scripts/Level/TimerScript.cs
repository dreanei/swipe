﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerScript : MonoBehaviour {

    private float size = 0f;

    private const float k = 0.86f;

    public LevelScript levelScript;
	
	void Start () {
        size = Screen.width * k;
    }
	
	void Update () {
        var rect = GetComponent<RectTransform>();
        rect.position = new Vector3(size/2 + (Screen.width - size)/2 - size*levelScript.timer.PersetGone(), rect.position.y, rect.position.z);
	}
}
