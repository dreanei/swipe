﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RemainedCountScript : MonoBehaviour {

    public LevelScript levelScript;

	void Update () {
        GetComponent<Text>().text = (levelScript.level.Pictures.Count-levelScript.currentPicture).ToString();
	}
}
