﻿using Assets.Scripts.src.Swipe;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FalseButton : MonoBehaviour {

    public SwipeScript swipeScript;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(delegate
        {
            swipeScript.Left();
        });
    }
}
