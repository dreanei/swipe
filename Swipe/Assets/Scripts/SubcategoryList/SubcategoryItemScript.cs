﻿using Assets.Scripts.src;
using Assets.Scripts.src.Models;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SubcategoryItemScript : MonoBehaviour {

    public Subcategory subcategory;

    public void setSubcategory(Subcategory subcategory)
    {
        this.subcategory = subcategory;
        GetComponentsInChildren<Image>()[2].sprite = subcategory.Sprite;
        foreach (TextMeshProUGUI text in GetComponentsInChildren<TextMeshProUGUI>())
        {
            if (text.name.Equals("TextPassed")) text.gameObject.SetActive(subcategory.State.Equals(State.PASSED));
            if (text.name.Equals("TextAvaliable")) text.gameObject.SetActive(subcategory.State.Equals(State.UNLOCKED));
            if (text.name.Equals("TextDisable")) text.gameObject.SetActive(subcategory.State.Equals(State.LOCKED) || !subcategory.IsLoaded);
            text.text = subcategory.Title;
        }

        if (!subcategory.State.Equals(State.LOCKED))
        {
            GetComponentsInChildren<Image>()[3].gameObject.SetActive(false);
            if (subcategory.IsLoaded)
                GetComponentsInChildren<Image>()[3].gameObject.SetActive(false);
        }
        else if (subcategory.IsLoaded)
            GetComponentsInChildren<Image>()[4].gameObject.SetActive(false);

        if (subcategory.State.Equals(State.LOCKED) || !subcategory.IsLoaded) GetComponentsInChildren<Image>()[2].gameObject.SetActive(false);

        GetComponentInChildren<Button>().onClick.AddListener(delegate
        {
            Selected.subcategory = subcategory;
            SceneManager.LoadScene("LevelListScene", LoadSceneMode.Single);
        });
    }

}
