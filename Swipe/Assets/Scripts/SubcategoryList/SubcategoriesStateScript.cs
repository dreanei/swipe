﻿using Assets.Scripts.src;
using Assets.Scripts.src.Models;
using Assets.Scripts.src.Repository;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SubcategoriesStateScript : MonoBehaviour {

    private List<Subcategory> subcategories;

    // Use this for initialization
    void Start()
    {
        if (Selected.category == null) Selected.Fix();
        subcategories = Repository.GetSubcategories(Selected.category);
        int passed = subcategories.Where(subcategory => subcategory.State == State.PASSED).Count();
        GetComponent<Text>().text = passed + "/" + subcategories.Count;
    }
}
