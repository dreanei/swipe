﻿using Assets.Scripts.src;
using Assets.Scripts.src.Repository;
using UnityEngine;

public class SubcategoryListScript : MonoBehaviour {

    public SubcategoryItemScript ItemPrefab;

    void Start()
    {
        if (Selected.category == null) Selected.Fix();
        var itemHeight = Screen.width / 4f;
        var subcategories = Repository.GetSubcategories(Selected.category);
        var rect = GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, itemHeight * subcategories.Count);
        //transform.position = Vector2.zero;

        for (int i = 0; i < subcategories.Count; i++)
        {
            var subcategory = subcategories[i];
            var item = Instantiate(ItemPrefab.gameObject, transform, true);
            var offset = -itemHeight * i - itemHeight / 5.5f;
            item.transform.position = new Vector3(
                    item.transform.position.x,
                    offset,
                    item.transform.position.z
                );
            item.GetComponent<SubcategoryItemScript>().setSubcategory(subcategory);
        }
        ItemPrefab.gameObject.SetActive(false);
    }
}
